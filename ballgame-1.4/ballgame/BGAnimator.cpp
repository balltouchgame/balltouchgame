/**
 *  This file is part of Ball Game
 *
 *  Copyright (C) 2011 Csaba Kertész (csaba.kertesz@gmail.com)
 *
 *  Ball Game is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  Ball Game is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 */

#include "BGAnimator.hpp"

#include <qgraphicsitem.h>

BGAnimator::BGAnimator(QGraphicsItem& item, AnimType anim_type, float target_value, int duration) :
  Item(item), Animation(anim_type), TargetValue(target_value), Duration(duration)
{
  Timer.setSingleShot(false);
  Timer.setInterval(10);
  connect(&Timer, SIGNAL(timeout()), this, SLOT(AnimTick()));

  if (Animation == OpacityAnim)
    StartValue = Item.opacity();
  if (Animation == PositionXAnim)
    StartValue = Item.pos().x();
  if (Animation == PositionYAnim)
    StartValue = Item.pos().y();
  Timer.start();
  Clock.start();
}


void BGAnimator::AnimTick()
{
  if (Clock.elapsed() > Duration)
  {
    if (Animation == OpacityAnim)
    {
      Item.setOpacity(TargetValue);
      if (TargetValue == 0.0)
        Item.hide();
    }
    if (Animation == PositionXAnim || Animation == PositionYAnim)
    {
      QPointF Pos = Item.pos();

      if (Animation == PositionXAnim)
        Item.setPos(TargetValue, Pos.y());
      if (Animation == PositionYAnim)
        Item.setPos(Pos.x(), TargetValue);
    }
    deleteLater();
    return;
  }

  if (Animation == OpacityAnim)
    Item.setOpacity(StartValue+(float)Clock.elapsed()*(TargetValue-StartValue) / Duration);

  if (Animation == PositionXAnim || Animation == PositionYAnim)
  {
    QPointF Pos = Item.pos();

    if (Animation == PositionXAnim)
      Item.setPos(StartValue+(float)Clock.elapsed()*(TargetValue-StartValue) / Duration, Pos.y());
    if (Animation == PositionYAnim)
      Item.setPos(Pos.x(), StartValue+(float)Clock.elapsed()*(TargetValue-StartValue) / Duration);
  }
}
