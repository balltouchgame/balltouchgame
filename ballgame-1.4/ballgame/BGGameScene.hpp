/**
 *  This file is part of Ball Game
 *
 *  Copyright (C) 2011 Csaba Kertész (csaba.kertesz@gmail.com)
 *
 *  Ball Game is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  Ball Game is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 */

#ifndef __BGGameScene_hpp__
#define __BGGameScene_hpp__

#include "BGScene.hpp"

#include <QGraphicsItemAnimation>
#include <QGraphicsSimpleTextItem>
#include <QTime>
#include <QTimer>

class BGButtonGroup;
namespace Phonon
{
class MediaObject;
}

class BGBall : public QObject, public QGraphicsPixmapItem
{
  Q_OBJECT
public:
  BGBall(int size, int width, int height);
  virtual ~BGBall() {};

  inline int GetCollisions()
  {
    return Collisions;
  }
signals:
  void Stopped();
  void Kicked();
  void Hit();

private slots:
  void PositionUpdate();

protected:
  virtual void mousePressEvent(QGraphicsSceneMouseEvent* event);
  virtual void mouseReleaseEvent(QGraphicsSceneMouseEvent* event);

  QPoint ForceStart;
  int ForceVectorX;
  int ForceVectorY;
  int ForceCounter;
  QTimer UpdateTimer;
  int Size;
  int Width;
  int Height;
  int Collisions;
};


class BGText : public QObject, public QGraphicsSimpleTextItem
{
  Q_OBJECT
public:
  BGText(int width, int height);

  void ShowText(const QString& text);

private slots:
  void SetFading();

private:
  int Width;
  int Height;
  QTimer FadingTimer;
  QTime FadingClock;
};


class BGGameScene : public BGScene
{
  Q_OBJECT
public:
  BGGameScene();
  virtual ~BGGameScene();

signals:
  void Finished();

public slots:
  virtual void Show();

  void StartGame();

private slots:
  void ShowLevel();
  void ShowGameOver();
  void ShowWin();
  void UpdateStats();
  void ResetGame();
  void NewBall();
  void NewHole();
  void BallStopped();
  void TapAndHold();
  void FocusLost();
  void FocusGained();
  void MusicFinished();
  void PlayBallHitSound();
  void PlayKickSound();
  void Finish();

  void SetMusicPixmap();
  void ToggleMusic();
  void BackToTheGame();

private:
  void PlayClapsSound();

  BGBall* RedBall;
  QGraphicsPixmapItem* Hole;
  QGraphicsPixmapItem* Logo;
  BGText* LevelText;
  QGraphicsSimpleTextItem* StatText;
  BGButton* MusicButton;
  BGButtonGroup GameMenu;
  Phonon::MediaObject* Music;
  Phonon::MediaObject* BallKickSound;
  Phonon::MediaObject* BallHitSound;
  Phonon::MediaObject* GameOverSound;
  Phonon::MediaObject* GameWinSound;
  Phonon::MediaObject* ClapsSounds[3];
  bool MusicPlayed;
  int BallSize;
  int HoleX;
  int HoleY;
  int Level;
  int Score;
  int Balls;
  int Attempts;
  int Hits;
};

#endif
