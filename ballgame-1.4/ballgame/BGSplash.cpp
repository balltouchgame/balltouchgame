/**
 *  This file is part of Ball Game
 *
 *  Copyright (C) 2011 Csaba Kertész (csaba.kertesz@gmail.com)
 *
 *  Ball Game is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  Ball Game is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 */

#include "BGSplash.hpp"

#include "stdio.h"

#include <QDebug>
#include <QFile>
#include <qsplashscreen.h>
#include <QTimer>

#include "BGFeedback.hpp"
#include "BGHelper.hpp"

BGSplash::BGSplash() : QSplashScreen(), Timer(NULL)
{
  setPixmap(*LoadPixmap("splash_screen.png"));
}


BGSplash::~BGSplash()
{
  if (Timer)
  {
    if (Timer->isActive())
      Timer->stop();
    delete Timer;
    Timer = NULL;
  }
}


void BGSplash::showEvent(QShowEvent* event)
{
  Q_UNUSED(event)
  if (!Timer)
    Timer = new QTimer();
  Timer->setSingleShot(true);
  Timer->start(1500);
  connect(Timer, SIGNAL(timeout()), this, SLOT(hide()));
  BGFeedback* Feedback = new BGFeedback(LowFeedback, 50);

  Feedback->Play();
}


void BGSplash::hideEvent(QHideEvent* event)
{
  Q_UNUSED(event)
  emit Disappearing();
}
