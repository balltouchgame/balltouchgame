TEMPLATE = app
QT += core gui phonon

RESOURCES = data.qrc

equals(QT_ARCH, arm) | equals(QT_ARCH, armv6) {
  maemo5 {
    CONFIG += qdbus
    message ("Using QtDBus for proper minimizing the app on N900")
  } else {
    DEFINES += USEQTMOBILITY
    CONFIG += mobility
    MOBILITY += feedback
    message ("Using Qt Mobility (for haptics)")
  }
}

TARGET = 
DEPENDPATH += .
INCLUDEPATH += .
SOURCES += BGTutorialScene.cpp \
    BGCreditsScene.cpp \
    BGAnimator.cpp \
    BGFeedback.cpp \
    BGGameScene.cpp \
    BGMainMenuScene.cpp \
    BGHelper.cpp \
    BGScene.cpp \
    BGSplash.cpp \
    BGState.cpp \
    BGAppController.cpp \
    main.cpp
HEADERS += BGTutorialScene.hpp \
    BGCreditsScene.hpp \
    BGAnimator.hpp \
    BGFeedback.hpp \
    BGGameScene.hpp \
    BGMainMenuScene.hpp \
    BGHelper.hpp \
    BGScene.hpp \
    BGSplash.hpp \
    BGState.hpp \
    BGAppController.hpp

equals(RPM_BUILD, on) {
meegouxapp.files = ballgame
meegouxapp.path = /opt/com.kecsap.ballgame

INSTALLS += meegouxapp
} else {
standardapp.files = ballgame
standardapp.path = /usr/bin

INSTALLS += standardapp
}
