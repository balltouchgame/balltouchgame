/**
 *  This file is part of Ball Game
 *
 *  Copyright (C) 2011 Csaba Kertész (csaba.kertesz@gmail.com)
 *
 *  Ball Game is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  Ball Game is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 */

#ifndef __BGMainMenuScene_hpp__
#define __BGMainMenuScene_hpp__

#include "BGScene.hpp"

class QGraphicsPixmapItem;
class BGButton;

class BGMainMenuScene : public BGScene
{
  Q_OBJECT
public:
  BGMainMenuScene();
  virtual ~BGMainMenuScene();

public slots:
  void ShowMenu();
  void HideMenu();

signals:
  void RequestNewGame();
  void RequestTutorial();
  void RequestCredits();

private slots:
  void ToggleAudio();

private:
  void SetAudioPixmap();

  QGraphicsPixmapItem* GameTitle;
  BGButton* AudioButton;
  BGButtonGroup Buttons;
};

#endif
