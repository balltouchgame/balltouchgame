/**
 *  This file is part of Ball Game
 *
 *  Copyright (C) 2011 Csaba Kertész (csaba.kertesz@gmail.com)
 *
 *  Ball Game is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  Ball Game is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 */

#include "BGScene.hpp"

#include "stdio.h"

#include <QApplication>
#include <QDesktopWidget>
#include <QGraphicsView>
#include <QMouseEvent>
#ifdef Q_WS_MAEMO_5
#include <QtDBus/QtDBus>
#endif

#include <phonon/MediaObject>

#include "BGAnimator.hpp"
#include "BGFeedback.hpp"
#include "BGHelper.hpp"

Phonon::MediaObject* BGButton::PressSound = NULL;

BGButton::BGButton() : QGraphicsPixmapItem()
{
  setOpacity(0.0);
  if (!PressSound)
  {
    PressSound = LoadAudio("click.wav");
  }
}


BGButton::~BGButton()
{
}


void BGButton::Show()
{
  show();
  new BGAnimator(*this, OpacityAnim, 1.0, 500);
//  QPointF CurrentPos = pos();

//  setPos(QApplication::desktop()->screenGeometry().width(), CurrentPos.y());
//  new BGAnimator(*this, PositionXAnim, CurrentPos.x(), 600);
}


void BGButton::Hide()
{
  new BGAnimator(*this, OpacityAnim, 0.0, 500);
//  new BGAnimator(*this, PositionXAnim, QApplication::desktop()->screenGeometry().width(), 300);
}


void BGButton::mousePressEvent(QGraphicsSceneMouseEvent* event)
{
  new BGAnimator(*this, OpacityAnim, 0.7, 65);
  QTimer::singleShot(70, this, SLOT(ReleaseAnim()));

  if (AudioEnabled)
  {
    PressSound->setCurrentSource(GetFileName("click.wav"));
    PressSound->play();
  }
  // Delay the press event a bit to get the click sound in sync
  QTimer::singleShot(40, this, SLOT(HapticClick()));
  QTimer::singleShot(40, this, SIGNAL(Clicked()));
  QGraphicsItem::mousePressEvent(event);
}


void BGButton::mouseReleaseEvent(QGraphicsSceneMouseEvent* event)
{
  // FIXME: Hmmmm it does not work...
  QGraphicsItem::mouseReleaseEvent(event);
}


void BGButton::HapticClick()
{
  BGFeedback* Feedback = new BGFeedback(MediumFeedback, 40);

  Feedback->Play();
}


void BGButton::ReleaseAnim()
{
  new BGAnimator(*this, OpacityAnim, 1.0, 65);
}


BGButtonGroup::~BGButtonGroup()
{
  for (int i = 0; i < Buttons.size(); ++i)
  {
    delete Buttons[i];
  }
  Buttons.clear();
}


void BGButtonGroup::Show()
{
  QPointF TempPoint = TopLeft;

  for (int i = 0; i < Buttons.size(); ++i)
  {
    Buttons[i]->setPos(TempPoint);
    QTimer::singleShot(100*i, Buttons[i], SLOT(Show()));
    TempPoint.setY(TempPoint.y()+50);
  }
}


void BGButtonGroup::Hide()
{
  for (int i = 0; i < Buttons.size(); ++i)
  {
    Buttons[i]->Hide();
  }
}


BGView::BGView() : QGraphicsView()
{
  Timer.setSingleShot(true);
  Timer.setInterval(600);
  connect(&Timer, SIGNAL(timeout()), this, SIGNAL(TapAndHold()));
}


void BGView::mouseMoveEvent(QMouseEvent* event)
{
  // Don't be too strict
  if (qAbs(event->pos().x()-LastPressPoint.x()) > 20 || 
      qAbs(event->pos().y()-LastPressPoint.y()) > 20)
    Timer.stop();
  QGraphicsView::mouseMoveEvent(event);
}


void BGView::mousePressEvent(QMouseEvent* event)
{
  Timer.start();
  LastPressPoint = event->pos();
  QGraphicsView::mousePressEvent(event);
}


void BGView::mouseReleaseEvent(QMouseEvent* event)
{
  Timer.stop();
  QGraphicsView::mouseReleaseEvent(event);
}


void BGView::focusInEvent(QFocusEvent* event)
{
  emit FocusIn();
  QGraphicsView::focusInEvent(event);
}


void BGView::focusOutEvent(QFocusEvent* event)
{
  emit FocusLost();
  QGraphicsView::focusOutEvent(event);
}


void BGView::Minimize()
{
#ifdef Q_WS_MAEMO_5
  QDBusConnection Connection = QDBusConnection::sessionBus();
  QDBusMessage Message = QDBusMessage::createSignal("/","com.nokia.hildon_desktop","exit_app_view");

  Connection.send(Message);
#else
  showMinimized();
#endif
}


BGView* BGScene::View = NULL;

BGScene::BGScene() : QGraphicsScene(), Width(0), Height(0)
{
  QDesktopWidget* DesktopWidget = QApplication::desktop();

  Width = DesktopWidget->screenGeometry().width();
  Height = DesktopWidget->screenGeometry().height();
  if (!View)
  {
    View = new BGView;
    View->setAttribute(Qt::WA_AcceptTouchEvents);
    View->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    View->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
//    View->setResizeAnchor(QGraphicsView::AnchorViewCenter);
//    View->setViewportUpdateMode(QGraphicsView::NoViewportUpdate);

    View->setRenderHint(QPainter::Antialiasing, false);
    View->setRenderHint(QPainter::TextAntialiasing, false);
    View->setRenderHint(QPainter::SmoothPixmapTransform, false);
    View->setRenderHint(QPainter::HighQualityAntialiasing, false);
    View->setRenderHint(QPainter::NonCosmeticDefaultPen, false);
    View->setOptimizationFlag(QGraphicsView::DontClipPainter, true);
    View->setOptimizationFlag(QGraphicsView::DontSavePainterState, false);
    View->setOptimizationFlag(QGraphicsView::DontAdjustForAntialiasing, true);
  }

  QLinearGradient Gradient(0, 0, Width, Height);

  Gradient.setColorAt(0, QColor(0x5c, 0xb3, 0xcf, 255));
  Gradient.setColorAt(1, QColor(255, 255, 255, 255));
  setBackgroundBrush(Gradient);
  // Set the display resolution like scene coordinate system
  setSceneRect(0, 0, Width, Height);
}


BGScene::~BGScene()
{
  delete View;
  View = NULL;
}


void BGScene::Show()
{
  if (!View->isFullScreen())
    View->showFullScreen();
  View->setScene(this);
}


void BGScene::Hide()
{
  View->hide();
}
