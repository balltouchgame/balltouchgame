/**
 *  This file is part of Ball Game
 *
 *  Copyright (C) 2011 Csaba Kertész (csaba.kertesz@gmail.com)
 *
 *  Ball Game is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  Ball Game is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 */

#ifndef __BGHelper_hpp__
#define __BGHelper_hpp__

#include <QObject>

class QPixmap;
namespace Phonon
{
class MediaObject;
class AudioOutput;
}

extern bool MusicEnabled;
extern bool AudioEnabled;

class InitAudioVolume : public QObject
{
  Q_OBJECT

public:
  InitAudioVolume(Phonon::MediaObject& audio, Phonon::AudioOutput& output, float volume);
  virtual ~InitAudioVolume() {};

private slots:
  void PlayFinished();

private:
  Phonon::AudioOutput& AudioOutput;
  float TargetVolume;
};

QString GetFileName(const QString& file_name);
QPixmap* LoadPixmap(const QString& file_name);
Phonon::MediaObject* LoadAudio(const QString& file_name, float volume = 0.05);

#endif
