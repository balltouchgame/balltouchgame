/**
 *  This file is part of Ball Game
 *
 *  Copyright (C) 2011 Csaba Kertész (csaba.kertesz@gmail.com)
 *
 *  Ball Game is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  Ball Game is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 */

#include "BGHelper.hpp"

#include <qfile.h>
#include <qfileinfo.h>
#include <qpixmap.h>
#include <qsound.h>

#include <phonon/MediaObject>
#include <phonon/Path>
#include <phonon/AudioOutput>
#include <phonon/Global>

bool MusicEnabled = true;
bool AudioEnabled = true;

InitAudioVolume::InitAudioVolume(Phonon::MediaObject& audio, Phonon::AudioOutput& output, float volume) :
  AudioOutput(output), TargetVolume(volume)
{
  connect(&audio, SIGNAL(finished()), this, SLOT(PlayFinished()));
}


void InitAudioVolume::PlayFinished()
{
  AudioOutput.setVolume(TargetVolume);
  deleteLater();
}


QString GetFileName(const QString& file_name)
{
  if (QFile(file_name).exists())
    return QFileInfo(file_name).canonicalFilePath();
  else
  if (QFile("../data/"+file_name).exists())
    return QFileInfo("../data/"+file_name).canonicalFilePath();
  else
  if (QFile("/usr/share/ballgame/"+file_name).exists())
    return QFileInfo("/usr/share/ballgame/"+file_name).canonicalFilePath();
  else
  if (QFile("/opt/com.kecsap.ballgame/"+file_name).exists())
    return QFileInfo("/opt/com.kecsap.ballgame/"+file_name).canonicalFilePath();

  return ":/data/"+file_name;
}


QPixmap* LoadPixmap(const QString& file_name)
{
  return new QPixmap(GetFileName(file_name));
}


Phonon::MediaObject* LoadAudio(const QString& file_name, float volume)
{
  Phonon::MediaObject* MediaObject = new Phonon::MediaObject();
  Phonon::AudioOutput* AudioOutput = new Phonon::AudioOutput(Phonon::MusicCategory);

  Phonon::createPath(MediaObject, AudioOutput);

  MediaObject->setCurrentSource(GetFileName(file_name));
#ifdef __arm__
  // Kind a hack for handsets to cache the smaller audio files
  if (!file_name.contains(".ogg"))
  {
    // Do a silent play to get the sample into the cache
    AudioOutput->setVolume(0);
    MediaObject->play();
    new InitAudioVolume(*MediaObject, *AudioOutput, volume);
  } else {
    AudioOutput->setVolume(volume);
  }
#else
  AudioOutput->setVolume(volume);
#endif
  return MediaObject;
}
