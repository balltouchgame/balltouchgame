/**
 *  This file is part of Ball Game
 *
 *  Copyright (C) 2011 Csaba Kertész (csaba.kertesz@gmail.com)
 *
 *  Ball Game is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  Ball Game is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 */

#include "BGGameScene.hpp"

#include "stdio.h"

#include <QAction>
#include <QApplication>
#include <QGraphicsEllipseItem>
#include <QGraphicsSceneMouseEvent>

#include <phonon/MediaObject>

#include "BGFeedback.hpp"
#include "BGHelper.hpp"

BGBall::BGBall(int size, int width, int height) : QObject(), QGraphicsPixmapItem(),
  ForceVectorX(-1), ForceVectorY(-1), Size(size), Width(width), Height(height),
  Collisions(0)
{
  UpdateTimer.setSingleShot(false);
  UpdateTimer.setInterval(30);
  UpdateTimer.stop();
  connect(&UpdateTimer, SIGNAL(timeout()), this, SLOT(PositionUpdate()));
}


void BGBall::PositionUpdate()
{
  int NewPosX = pos().x()+ForceVectorX;
  int NewPosY = pos().y()+ForceVectorY;

  if (NewPosY < 0)
  {
    NewPosY = -NewPosY;
    ForceVectorY = -ForceVectorY;
    Collisions++;
    emit Kicked();
  }
  if (NewPosX < 0)
  {
    NewPosX = -(NewPosX);
    ForceVectorX = -ForceVectorX;
    Collisions++;
    emit Kicked();
  }
  if (NewPosY > Height-Size)
  {
    NewPosY = Height-Size-(NewPosY-Height+Size);
    ForceVectorY = -ForceVectorY;
    Collisions++;
    emit Kicked();
  }
  if (NewPosX > Width-Size)
  {
    NewPosX = Width-Size-(NewPosX-Width+Size);
    ForceVectorX = -ForceVectorX;
    Collisions++;
    emit Kicked();
  }

  setPos(NewPosX, NewPosY);
  ForceCounter++;
  if (ForceCounter < 5)
  {
    ForceVectorX = (int)((float)ForceVectorX / 1.3);
    ForceVectorY = (int)((float)ForceVectorY / 1.3);
  } else {
    ForceVectorX = (int)((float)ForceVectorX / 1.05);
    ForceVectorY = (int)((float)ForceVectorY / 1.05);
  }
  if (ForceVectorX == 0 && ForceVectorY == 0)
  {
    UpdateTimer.stop();
    emit Stopped();
  }
}


void BGBall::mousePressEvent(QGraphicsSceneMouseEvent* event)
{
  // Skip the hits during a movement
  if (UpdateTimer.isActive())
    return;
  // Remember for the start of the force
  ForceStart = event->lastScreenPos();
}


void BGBall::mouseReleaseEvent(QGraphicsSceneMouseEvent* event)
{
  // Skip the hits during a movement
  if (UpdateTimer.isActive())
    return;
  ForceVectorX = (int)((float)(event->lastScreenPos().x()-ForceStart.x()) / 1.5);
  ForceVectorY = (int)((float)(event->lastScreenPos().y()-ForceStart.y()) / 1.5);
  ForceCounter = 0;
  Collisions = 0;
  UpdateTimer.start();
  emit Hit();
}


BGText::BGText(int width, int height) : QObject(), QGraphicsSimpleTextItem(),
  Width(width), Height(height)
{
  setOpacity(0.0);
  FadingTimer.setSingleShot(false);
  FadingTimer.setInterval(20);
  connect(&FadingTimer, SIGNAL(timeout()), this, SLOT(SetFading()));
}


void BGText::ShowText(const QString& text)
{
  setText(text);
  setPos(Width / 2-boundingRect().width() / 2, Height / 2-boundingRect().height() / 2);
  show();
  FadingTimer.start();
  FadingClock.start();
}


void BGText::SetFading()
{
  int ElapsedTime = FadingClock.elapsed();

  if (ElapsedTime < 500)
  {
    setOpacity(0.0+(float)ElapsedTime / 500);
  } else
  if (ElapsedTime < 1000)
  {
    // Nothing to do
  } else
  if (ElapsedTime < 1300)
  {
    setOpacity(1.0-(float)(ElapsedTime-1000) / 300);
  } else
  if (ElapsedTime >= 1500)
  {
    setOpacity(0.0);
    hide();
    FadingTimer.stop();
  }
}


BGGameScene::BGGameScene() : BGScene()
{
  BallSize = Height < Width ? Height / 4 : Width / 4;

  // Level notification
  LevelText = new BGText(Width, Height);
  QFont Font = LevelText->font();

  Font.setPixelSize(50);
  LevelText->setFont(Font);
  LevelText->setZValue(10);
  addItem(LevelText);

  // Statistics
  StatText = new QGraphicsSimpleTextItem();
  Font = StatText->font();

  Font.setPixelSize(25);
  StatText->setFont(Font);
  StatText->setPos(5.0, 5.0);
  addItem(StatText);

  QPixmap* RedBallPixmap = LoadPixmap("red_ball.png");

  RedBall = new BGBall(BallSize, Width, Height);
  connect(RedBall, SIGNAL(Stopped()), this, SLOT(BallStopped()));
  connect(RedBall, SIGNAL(Hit()), this, SLOT(PlayBallHitSound()));
  connect(RedBall, SIGNAL(Kicked()), this, SLOT(PlayKickSound()));
  *RedBallPixmap = RedBallPixmap->copy(30, 30, RedBallPixmap->height()-60, RedBallPixmap->height()-60);
  *RedBallPixmap = RedBallPixmap->scaled(BallSize, BallSize);
  RedBall->setPixmap(*RedBallPixmap);
  delete RedBallPixmap;
  RedBall->setZValue(0);
  addItem(RedBall);

  QPixmap* HolePixmap = LoadPixmap("hole.png");

  Hole = new QGraphicsPixmapItem;
  *HolePixmap = HolePixmap->copy(30, 30, HolePixmap->height()-60, HolePixmap->height()-60);
  *HolePixmap = HolePixmap->scaled(BallSize, BallSize);
  Hole->setZValue(-10);
  Hole->setPixmap(*HolePixmap);
  delete HolePixmap;
  addItem(Hole);

  QPixmap* LogoPixmap = LoadPixmap("game_title.png");

  Logo = new QGraphicsPixmapItem;
  Logo->setZValue(-10);
  Logo->setOpacity(0.5);
  Logo->setPixmap(*LogoPixmap);
  addItem(Logo);
  Logo->setPos(0, Height-LogoPixmap->height());
  delete LogoPixmap;

  // Sound and music
  BallKickSound = LoadAudio("ball_kick.wav");
  BallHitSound = LoadAudio("ball_hit.wav");
  GameOverSound = LoadAudio("game_over.wav");
  GameWinSound = LoadAudio("game_win.wav");
  ClapsSounds[0] = LoadAudio("claps1.wav");
  ClapsSounds[1] = LoadAudio("claps2.wav");
  ClapsSounds[2] = LoadAudio("claps3.wav");
  Music = LoadAudio("music.ogg");
  connect(Music, SIGNAL(finished()), this, SLOT(MusicFinished()));
  MusicPlayed = false;

  // Game menu

  // Continue button
  QPixmap* ContinueButtonPixmap = LoadPixmap("continue_button.png");
  BGButton* ContinueButton = new BGButton;

  ContinueButton->setPixmap(*ContinueButtonPixmap);
  connect(ContinueButton, SIGNAL(Clicked()), this, SLOT(BackToTheGame()));
  connect(ContinueButton, SIGNAL(Clicked()), &GameMenu, SLOT(Hide()));
  addItem(ContinueButton);
  delete ContinueButtonPixmap;
  GameMenu.AddButton(*ContinueButton);
  // Set the top-left position of the button group
  GameMenu.SetTopLeft(QPointF(Width / 2-ContinueButton->pixmap().width() / 2, Height / 2-100));

  // Restart button
  QPixmap* RestartButtonPixmap = LoadPixmap("restart_button.png");
  BGButton* RestartButton = new BGButton;

  RestartButton->setPixmap(*RestartButtonPixmap);
  connect(RestartButton, SIGNAL(Clicked()), this, SLOT(StartGame()));
  connect(RestartButton, SIGNAL(Clicked()), &GameMenu, SLOT(Hide()));
  addItem(RestartButton);
  delete RestartButtonPixmap;
  GameMenu.AddButton(*RestartButton);

  // Music button
  MusicButton = new BGButton;

  connect(MusicButton, SIGNAL(Clicked()), this, SLOT(ToggleMusic()));
  addItem(MusicButton);
  GameMenu.AddButton(*MusicButton);

  // Minimize button
  QPixmap* MinimizeButtonPixmap = LoadPixmap("minimize_button.png");
  BGButton* MinimizeButton = new BGButton;

  MinimizeButton->setPixmap(*MinimizeButtonPixmap);
  connect(MinimizeButton, SIGNAL(Clicked()), View, SLOT(Minimize()));
  addItem(MinimizeButton);
  delete MinimizeButtonPixmap;
  GameMenu.AddButton(*MinimizeButton);

  // Main Menu button
  QPixmap* MainMenuButtonPixmap = LoadPixmap("main_menu_button.png");
  BGButton* MainMenuButton = new BGButton;

  MainMenuButton->setPixmap(*MainMenuButtonPixmap);
  connect(MainMenuButton, SIGNAL(Clicked()), this, SIGNAL(Finished()));
  connect(MainMenuButton, SIGNAL(Clicked()), &GameMenu, SLOT(Hide()));
  addItem(MainMenuButton);
  delete MainMenuButtonPixmap;
  GameMenu.AddButton(*MainMenuButton);

  // Pause/play again the music when focus changes
  connect(View, SIGNAL(FocusIn()), this, SLOT(FocusGained()));
  connect(View, SIGNAL(FocusLost()), this, SLOT(FocusLost()));
}


BGGameScene::~BGGameScene()
{
  delete BallKickSound;
  BallKickSound = NULL;
  delete BallHitSound;
  BallHitSound = NULL;
  delete GameOverSound;
  GameOverSound = NULL;
  delete GameWinSound;
  GameWinSound = NULL;
  for (int i = 0; i < 3; ++i)
  {
    delete ClapsSounds[i];
    ClapsSounds[i] = 0;
  }
  delete Music;
  Music = NULL;
}


void BGGameScene::Show()
{
  connect(View, SIGNAL(TapAndHold()), this, SLOT(TapAndHold()));
  connect(this, SIGNAL(Finished()), this, SLOT(Finish()));
  BGScene::Show();
  if (MusicEnabled && AudioEnabled && !MusicPlayed)
  {
    Music->play();
    MusicPlayed = true;
  }
}


void BGGameScene::StartGame()
{
  // Set the default values of the game
  ResetGame();
  QTimer::singleShot(300, this, SLOT(ShowLevel()));
  RedBall->show();
  Hole->show();
  // The ball can be disabled after the game menu
  if (!RedBall->isEnabled())
    RedBall->setEnabled(true);
  NewBall();
  NewHole();
  UpdateStats();
}


void BGGameScene::ShowLevel()
{
  LevelText->ShowText("Level "+QString::number(Level));
}


void BGGameScene::ShowGameOver()
{
  LevelText->ShowText("Game Over");
  if (AudioEnabled)
  {
    GameOverSound->setCurrentSource(GetFileName("game_over.wav"));
    GameOverSound->play();
  }
}


void BGGameScene::ShowWin()
{
  LevelText->ShowText("You are the winner!");
  if (AudioEnabled)
  {
    GameWinSound->setCurrentSource(GetFileName("game_win.wav"));
    GameWinSound->play();
  }
}


void BGGameScene::UpdateStats()
{
  QString StatStr;

  StatStr = "Score: ";
  if (Score < 10)
    StatStr += "0";
  if (Score < 100)
    StatStr += "0";
  if (Score < 1000)
    StatStr += "0";
  if (Score < 10000)
    StatStr += "0";
  StatStr += QString::number(Score)+"  ";
  StatStr += "Balls: "+QString::number(Balls)+"  ";
  StatStr += "Attempts: "+QString::number(Attempts);

  StatText->setText(StatStr);
}


void BGGameScene::ResetGame()
{
  Attempts = 5;
  Score = 0;
  Level = 1;
  Balls = 3;
  Hits = 0;
}


void BGGameScene::NewBall()
{
  int BallX;
  int BallY;

  BallX = HoleX;
  BallY = HoleY;
  while (qAbs(BallX-HoleX) < BallSize*2 && qAbs(BallY-HoleY) < BallSize*2)
  {
    BallX = rand() % (Width-BallSize);
    BallY = rand() % (Height-BallSize);
  }
  RedBall->setPos(BallX, BallY);
}


void BGGameScene::NewHole()
{
  int BallX = (int)RedBall->pos().x()+BallSize / 2;;
  int BallY = (int)RedBall->pos().y()+BallSize / 2;;


  HoleX = BallX;
  HoleY = BallY;
  while (qAbs(BallX-HoleX) < BallSize*2 && qAbs(BallY-HoleY) < BallSize*2)
  {
    HoleX = BallSize / 2+rand() % (Width-BallSize);
    HoleY = BallSize / 2+rand() % (Height-BallSize);
  }
  Hole->setPos(HoleX-BallSize / 2, HoleY-BallSize / 2);
}


void BGGameScene::BallStopped()
{
  // Hit
  if (qAbs(RedBall->pos().x()-HoleX+BallSize / 2) < 30 &&
      qAbs(RedBall->pos().y()-HoleY+BallSize / 2) < 30)
  {
    PlayClapsSound();
    Score += 10+RedBall->GetCollisions()*50;
    // Bonus ball for >= 5 collisions
    if (RedBall->GetCollisions() >= 5)
      Balls++;
    Hits++;
    if (Hits == 5)
    {
      Level++;
      Hits = 0;
      Score += 100;
      if (Level == 6)
      {
        RedBall->hide();
        Hole->hide();
        QTimer::singleShot(1500, this, SIGNAL(Finished()));
        UpdateStats();
        ShowWin();
        return;
      }
      ShowLevel();
    }
    Attempts = 6-Level;
    NewBall();
    NewHole();
    UpdateStats();
    return;
  }

  Attempts--;

  if (Attempts == 0)
  {
    Balls--;
    if (Balls >= 0)
    {
      Attempts = 6-Level;
      NewBall();
    } else {
      // Game Over
      RedBall->hide();
      Hole->hide();
      ShowGameOver();
      QTimer::singleShot(1500, this, SIGNAL(Finished()));
      return;
    }
  }
  UpdateStats();
}


void BGGameScene::TapAndHold()
{
  SetMusicPixmap();
  GameMenu.Show();
  RedBall->setEnabled(false);
}


void BGGameScene::FocusLost()
{
  if (MusicEnabled && AudioEnabled && Music->state() != Phonon::PausedState)
    Music->pause();
}


void BGGameScene::FocusGained()
{
  // Play again after the music has been paused earlier.
  if (MusicEnabled && AudioEnabled && Music->state() != Phonon::PlayingState && MusicPlayed)
  {
    Music->play();
  }
}


void BGGameScene::MusicFinished()
{
  if (MusicEnabled && AudioEnabled)
  {
    Music->setCurrentSource(GetFileName("music.ogg"));
    Music->play();
  }
}


void BGGameScene::PlayBallHitSound()
{
  if (AudioEnabled)
  {
    BallHitSound->setCurrentSource(GetFileName("ball_hit.wav"));
    BallHitSound->play();
  }
  BGFeedback* Feedback = new BGFeedback(LowFeedback, 30);

  Feedback->Play();
}


void BGGameScene::PlayKickSound()
{
  if (AudioEnabled)
  {
    BallKickSound->setCurrentSource(GetFileName("ball_kick.wav"));
    BallKickSound->play();
  }
  BGFeedback* Feedback = new BGFeedback(HighFeedback, 60);

  Feedback->Play();
}


void BGGameScene::Finish()
{
  disconnect(View, SIGNAL(TapAndHold()), this, SLOT(TapAndHold()));
  disconnect(this, SIGNAL(Finished()), this, SLOT(Finish()));

  if (MusicEnabled && AudioEnabled)
  {
    Music->stop();
    MusicPlayed = false;
  }
}


void BGGameScene::PlayClapsSound()
{
  if (AudioEnabled)
  {
    int ClapsIndex = qrand() % 3;
    QString FileName = "claps"+QString::number(ClapsIndex+1)+".wav";

    ClapsSounds[ClapsIndex]->setCurrentSource(GetFileName(FileName));
    ClapsSounds[ClapsIndex]->play();
  }
  BGFeedback* Feedback = new BGFeedback(LowFeedback, 100);

  Feedback->Play();
}


void BGGameScene::SetMusicPixmap()
{
  if (!AudioEnabled)
  {
    MusicEnabled = false;
  }
  QPixmap* MusicButtonPixmap = MusicEnabled ? LoadPixmap("music_on_button.png") :
                               LoadPixmap("music_off_button.png");

  MusicButton->setPixmap(*MusicButtonPixmap);
  delete MusicButtonPixmap;
}


void BGGameScene::ToggleMusic()
{
  if (!AudioEnabled)
    return;

  MusicEnabled = !MusicEnabled;

  SetMusicPixmap();
  if (MusicEnabled)
  {
    Music->play();
  } else {
    Music->pause();
  }
}


void BGGameScene::BackToTheGame()
{
  GameMenu.Hide();
  RedBall->setEnabled(true);
}

