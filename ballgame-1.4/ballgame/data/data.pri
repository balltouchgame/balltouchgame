
# Common staff
icon.files = data/ballgame/ballgame.png
icon.path = /usr/share/icons

INSTALLS += icon

equals(RPM_BUILD, on) {
  # MeeGo installation
  meegouxdesktop.files = data/ballgame/meego-app-ballgame.desktop
  meegouxdesktop.path = /usr/share/applications

  INSTALLS += meegouxdesktop
} else {
  maemo5 {
    # N900 place of the desktop file. D'oh!
    n900desktop.files = data/ballgame/ballgame-n900.desktop
    n900desktop.path = /usr/share/applications/hildon

    INSTALLS += n900desktop
  } else {
    # Standard desktop file location
    standarddesktop.files = data/ballgame/ballgame.desktop
    standarddesktop.path = /usr/share/applications

    INSTALLS += standarddesktop
  }
}