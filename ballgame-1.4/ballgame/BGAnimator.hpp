/**
 *  This file is part of Ball Game
 *
 *  Copyright (C) 2011 Csaba Kertész (csaba.kertesz@gmail.com)
 *
 *  Ball Game is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  Ball Game is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 */

#ifndef __BGAnimator_hpp__
#define __BGAnimator_hpp__

#include <QObject>
#include <QTime>
#include <QTimer>

class QGraphicsItem;

typedef enum
{
  OpacityAnim = 0,
  PositionXAnim,
  PositionYAnim,
} AnimType;

class BGAnimator : public QObject
{
  Q_OBJECT

public:
  BGAnimator(QGraphicsItem& item, AnimType anim_type, float target_value, int duration = 300);
  virtual ~BGAnimator() {};

private slots:
  void AnimTick();

private:
  QGraphicsItem& Item;
  QTimer Timer;
  QTime Clock;
  AnimType Animation;
  float TargetValue;
  int Duration;
  float StartValue;
};

#endif
