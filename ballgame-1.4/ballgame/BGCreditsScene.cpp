/**
 *  This file is part of Ball Game
 *
 *  Copyright (C) 2011 Csaba Kertész (csaba.kertesz@gmail.com)
 *
 *  Ball Game is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  Ball Game is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 */

#include "BGCreditsScene.hpp"

#include "stdio.h"

#include <QApplication>

#include "BGAnimator.hpp"
#include "BGHelper.hpp"

BGCreditsScene::BGCreditsScene() : BGScene()
{
  // Credits
  CreditsTitle = new QGraphicsSimpleTextItem();
  QFont Font = CreditsTitle->font();

  Font.setPixelSize(50);
  CreditsTitle->setFont(Font);
  CreditsTitle->setPos(10.0, 10.0);
  CreditsTitle->setText("Credits");
  addItem(CreditsTitle);

  // Credits text
  CreditsText = new QGraphicsSimpleTextItem();
  Font = CreditsText->font();

  Font.setPixelSize(21);
  CreditsText->setFont(Font);
  CreditsText->setPos(10.0, 100.0);
  CreditsText->setText(QString("Copyright (C) 2011 Csaba Kertesz (csaba.kertesz@gmail.com).\n")+
                       "All rights reversed.\n"+
                       "The source code is available under GPL 2 license.\n\n"+
                       "Everything has been done from scratch in one week long.\n"+
                       "Built on Qt; Audacity and Gimp were used to produce/transform sounds and images.\n\n"
                       "Special thanks for the free raw sounds:\n"+
                       "Black Boe, totya, RHumphries, edbles, oniwe, TicTacShutUp, volivieri.\n\n"
                       "Thanks for the free music:\n"+
                       "piccolo trumpet swell by nickleus");
  addItem(CreditsText);

  // Main Menu button
  QPixmap* MainMenuButtonPixmap = LoadPixmap("main_menu_button.png");

  MainMenuButton = new BGButton;
  MainMenuButton->setPixmap(*MainMenuButtonPixmap);
  connect(MainMenuButton, SIGNAL(Clicked()), this, SIGNAL(Leave()));
  addItem(MainMenuButton);
  MainMenuButton->setPos(QPointF(Width-MainMenuButton->pixmap().width()-20, Height-MainMenuButton->pixmap().height()-20));
  delete MainMenuButtonPixmap;
}


BGCreditsScene::~BGCreditsScene()
{
  delete CreditsTitle;
  CreditsTitle = NULL;
  delete CreditsText;
  CreditsText = NULL;
  delete MainMenuButton;
  MainMenuButton = NULL;
}


void BGCreditsScene::ShowCredits()
{
  CreditsTitle->setOpacity(0.0);
  CreditsTitle->show();
  new BGAnimator(*CreditsTitle, OpacityAnim, 1.0);

  CreditsText->setOpacity(0.0);
  CreditsText->show();
  new BGAnimator(*CreditsText, OpacityAnim, 1.0);

  MainMenuButton->Show();
}


void BGCreditsScene::HideCredits()
{
  new BGAnimator(*CreditsTitle, OpacityAnim, 0.0, 300);
  new BGAnimator(*CreditsText, OpacityAnim, 0.0, 300);
  MainMenuButton->Hide();
}
