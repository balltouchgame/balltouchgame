/**
 *  This file is part of Ball Game
 *
 *  Copyright (C) 2011 Csaba Kertész (csaba.kertesz@gmail.com)
 *
 *  Ball Game is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  Ball Game is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 */

#include "BGMainMenuScene.hpp"

#include "stdio.h"

#include <QApplication>

#include "BGAnimator.hpp"
#include "BGHelper.hpp"

BGMainMenuScene::BGMainMenuScene() : BGScene()
{
  QPixmap* GameTitlePixmap = LoadPixmap("game_title.png");

  GameTitle = addPixmap(*GameTitlePixmap);
  GameTitle->setPos(Width / 2-GameTitlePixmap->width() / 2, Height / 2-150);
  delete GameTitlePixmap;

  // New Game button
  QPixmap* NewGameButtonPixmap = LoadPixmap("new_game_button.png");

  BGButton* NewGameButton = new BGButton;
  NewGameButton->setPixmap(*NewGameButtonPixmap);
  connect(NewGameButton, SIGNAL(Clicked()), this, SIGNAL(RequestNewGame()));
  addItem(NewGameButton);
  delete NewGameButtonPixmap;
  Buttons.AddButton(*NewGameButton);
  // Set the top-left position of the button group
  Buttons.SetTopLeft(QPointF(Width / 2-NewGameButton->pixmap().width() / 2, Height / 2-60));

  // Tutorial button
  QPixmap* TutorialButtonPixmap = LoadPixmap("tutorial_button.png");;

  BGButton* TutorialButton = new BGButton;
  TutorialButton->setPixmap(*TutorialButtonPixmap);
  connect(TutorialButton, SIGNAL(Clicked()), this, SIGNAL(RequestTutorial()));
  addItem(TutorialButton);
  delete TutorialButtonPixmap;
  Buttons.AddButton(*TutorialButton);

  // Audio button
  AudioButton = new BGButton;
  connect(AudioButton, SIGNAL(Clicked()), this, SLOT(ToggleAudio()));
  addItem(AudioButton);
  Buttons.AddButton(*AudioButton);

  // Credits button
  QPixmap* CreditsButtonPixmap = LoadPixmap("credits_button.png");;

  BGButton* CreditsButton = new BGButton;
  CreditsButton->setPixmap(*CreditsButtonPixmap);
  connect(CreditsButton, SIGNAL(Clicked()), this, SIGNAL(RequestCredits()));
  addItem(CreditsButton);
  delete CreditsButtonPixmap;
  Buttons.AddButton(*CreditsButton);

  // Quit button
  QPixmap* QuitButtonPixmap = LoadPixmap("quit_button.png");;

  BGButton* QuitButton = new BGButton;
  QuitButton->setPixmap(*QuitButtonPixmap);
  connect(QuitButton, SIGNAL(Clicked()), QApplication::instance(), SLOT(quit()));
  addItem(QuitButton);
  delete QuitButtonPixmap;
  Buttons.AddButton(*QuitButton);
}


BGMainMenuScene::~BGMainMenuScene()
{
  delete GameTitle;
  GameTitle = NULL;
}


void BGMainMenuScene::ShowMenu()
{
  GameTitle->setOpacity(0.0);
  SetAudioPixmap();
  GameTitle->show();
  new BGAnimator(*GameTitle, OpacityAnim, 1.0);
  QTimer::singleShot(300, &Buttons, SLOT(Show()));
}


void BGMainMenuScene::HideMenu()
{
  new BGAnimator(*GameTitle, OpacityAnim, 0.0, 300);
  Buttons.Hide();
}


void BGMainMenuScene::ToggleAudio()
{
  AudioEnabled = !AudioEnabled;
  SetAudioPixmap();
}


void BGMainMenuScene::SetAudioPixmap()
{
  QPixmap* AudioButtonPixmap = AudioEnabled ? LoadPixmap("audio_on_button.png") : LoadPixmap("audio_off_button.png");

  AudioButton->setPixmap(*AudioButtonPixmap);
  delete AudioButtonPixmap;
}
