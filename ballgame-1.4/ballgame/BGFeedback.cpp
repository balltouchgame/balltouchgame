/**
 *  This file is part of Ball Game
 *
 *  Copyright (C) 2011 Csaba Kertész (csaba.kertesz@gmail.com)
 *
 *  Ball Game is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  Ball Game is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 */

#include "BGFeedback.hpp"

#ifdef USEQTMOBILITY
#include <QFeedbackHapticsEffect>

QTM_USE_NAMESPACE
#endif

#ifdef Q_WS_MAEMO_5
#include <mce/mode-names.h>
#include <mce/dbus-names.h>
#endif

BGFeedback::BGFeedback(FeedbackType feedback_type, int duration) :
  Feedback(feedback_type), Duration(duration)
{
#ifdef Q_WS_MAEMO_5
  DBusInterface = new QDBusInterface(MCE_SERVICE, MCE_REQUEST_PATH,
                                     MCE_REQUEST_IF, QDBusConnection::systemBus(), this);
  DBusInterface->call(MCE_ENABLE_VIBRATOR);
#endif
}


BGFeedback::~BGFeedback()
{
#ifdef Q_WS_MAEMO_5
  delete DBusInterface;
  DBusInterface = NULL;
#endif
}


void BGFeedback::Play()
{
#ifdef USEQTMOBILITY
  // Play a haptic effect
  QFeedbackHapticsEffect HitEffect;

  if (Feedback == LowFeedback)
    HitEffect.setIntensity(0.2);
  if (Feedback == MediumFeedback)
    HitEffect.setIntensity(0.5);
  if (Feedback == HighFeedback)
    HitEffect.setIntensity(1.0);

  HitEffect.setDuration(Duration);
  HitEffect.start();

  QTimer::singleShot(1, this, SLOT(deleteLater()));
#endif

#ifdef Q_WS_MAEMO_5
  if (Feedback == LowFeedback)
    CurrentEffect = "PatternTouchscreen";
  if (Feedback == MediumFeedback)
    CurrentEffect = "PatternTouchscreen";
  if (Feedback == HighFeedback)
    CurrentEffect = "PatternIncomingCall";

  DBusInterface->call(MCE_ACTIVATE_VIBRATOR_PATTERN, qPrintable(CurrentEffect));
  QTimer::singleShot(Duration, this, SLOT(Stop()));
#endif
}


#ifdef Q_WS_MAEMO_5
void BGFeedback::Stop()
{
  DBusInterface->call(MCE_DEACTIVATE_VIBRATOR_PATTERN, qPrintable(CurrentEffect));
}
#endif

