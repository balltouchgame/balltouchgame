/**
 *  This file is part of Ball Game
 *
 *  Copyright (C) 2011 Csaba Kertész (csaba.kertesz@gmail.com)
 *
 *  Ball Game is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  Ball Game is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 */

#include "BGAppController.hpp"

#include "stdio.h"

#include <QDateTime>
#include <QStateMachine>

#include "BGCreditsScene.hpp"
#include "BGGameScene.hpp"
#include "BGMainMenuScene.hpp"
#include "BGSplash.hpp"
#include "BGState.hpp"
#include "BGTutorialScene.hpp"

BGAppController::BGAppController() : Splash(NULL), MainMenu(NULL), GamePlay(NULL)
{
  StateMachine = new QStateMachine();
  LoadingState = new BGState("Loading");
  MainMenuState = new BGState("MainMenu");
  GamePlayState = new BGState("GamePlay");
  CreditsState = new BGState("Credits");
  TutorialState = new BGState("Tutorial");
  StateMachine->addState(LoadingState);
  StateMachine->addState(MainMenuState);
  StateMachine->addState(GamePlayState);
  StateMachine->addState(CreditsState);
  StateMachine->addState(TutorialState);
  connect(LoadingState, SIGNAL(entered()), this, SLOT(NewState()));
  connect(MainMenuState, SIGNAL(entered()), this, SLOT(NewState()));
  connect(GamePlayState, SIGNAL(entered()), this, SLOT(NewState()));
  connect(CreditsState, SIGNAL(entered()), this, SLOT(NewState()));
  connect(TutorialState, SIGNAL(entered()), this, SLOT(NewState()));
  StateMachine->setInitialState(LoadingState);
  StateMachine->start();

  Splash = new BGSplash();
  MainMenu = new BGMainMenuScene();
  GamePlay = new BGGameScene();
  Credits = new BGCreditsScene();
  Tutorial = new BGTutorialScene();

  LoadingState->addTransition(Splash, SIGNAL(Disappearing()), MainMenuState);
  MainMenuState->addTransition(MainMenu, SIGNAL(RequestNewGame()), GamePlayState);
  MainMenuState->addTransition(MainMenu, SIGNAL(RequestCredits()), CreditsState);
  MainMenuState->addTransition(MainMenu, SIGNAL(RequestTutorial()), TutorialState);
  GamePlayState->addTransition(GamePlay, SIGNAL(Finished()), MainMenuState);
  CreditsState->addTransition(Credits, SIGNAL(Leave()), MainMenuState);
  TutorialState->addTransition(Tutorial, SIGNAL(Leave()), MainMenuState);

  // Set a new number as random seed
  qsrand(QDateTime::currentDateTime().toTime_t());
  srand(QDateTime::currentDateTime().toTime_t());
}


BGAppController::~BGAppController()
{
  delete StateMachine;
  StateMachine = NULL;
  delete Splash;
  Splash = NULL;
  delete MainMenu;
  MainMenu = NULL;
  delete GamePlay;
  GamePlay = NULL;
  delete Credits;
  Credits = NULL;
  delete Tutorial;
  Tutorial = NULL;
}


void BGAppController::NewState()
{
  if (LoadingState->IsActive())
  {
    Splash->show();
  }
  if (MainMenuState->IsActive())
  {
    MainMenu->Show();
    MainMenu->ShowMenu();
  }
  if (GamePlayState->IsActive())
  {
    if (MainMenu->isActive())
      MainMenu->HideMenu();
    QTimer::singleShot(300, GamePlay, SLOT(Show()));
    QTimer::singleShot(300, GamePlay, SLOT(StartGame()));
  }
  if (CreditsState->IsActive())
  {
    if (MainMenu->isActive())
      MainMenu->HideMenu();
    QTimer::singleShot(300, Credits, SLOT(Show()));
    QTimer::singleShot(300, Credits, SLOT(ShowCredits()));
  }
  if (TutorialState->IsActive())
  {
    if (MainMenu->isActive())
      MainMenu->HideMenu();
    QTimer::singleShot(300, Tutorial, SLOT(Show()));
    QTimer::singleShot(300, Tutorial, SLOT(ShowTutorial()));
  }
}
