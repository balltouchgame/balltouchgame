/**
 *  This file is part of Ball Game
 *
 *  Copyright (C) 2011 Csaba Kertész (csaba.kertesz@gmail.com)
 *
 *  Ball Game is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  Ball Game is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 */

#ifndef __BGScene_hpp__
#define __BGScene_hpp__

#include <QGraphicsPixmapItem>
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QObject>
#include <QTimer>

class QGraphicsPixmapItem;
namespace Phonon
{
class MediaObject;
}

class BGButton : public QObject, public QGraphicsPixmapItem
{
  Q_OBJECT
public:
  BGButton();
  virtual ~BGButton();

signals:
  void Clicked();

public slots:
  void Show();
  void Hide();

protected:
  virtual void mousePressEvent(QGraphicsSceneMouseEvent* event);
  virtual void mouseReleaseEvent(QGraphicsSceneMouseEvent* event);

private slots:
  void HapticClick();
  void ReleaseAnim();

private:
  static Phonon::MediaObject* PressSound;
};

class BGButtonGroup : public QObject
{
  Q_OBJECT
public:
  virtual ~BGButtonGroup();

  inline void AddButton(BGButton& button)
  {
    Buttons.push_back(&button);
  }
  inline void SetTopLeft(const QPointF& point)
  {
    TopLeft = point;
  }
  inline bool IsEmpty()
  {
    return Buttons.isEmpty();
  }
public slots:
  void Show();
  void Hide();

private:
  QVector<BGButton*> Buttons;
  QPointF TopLeft;
};

class BGView : public QGraphicsView
{
  Q_OBJECT

public:
  BGView();
  virtual ~BGView() { };

public slots:
  void Minimize();

signals:
  void TapAndHold();
  void FocusLost();
  void FocusIn();

protected:
  virtual void mouseMoveEvent(QMouseEvent* event);
  virtual void mousePressEvent(QMouseEvent* event);
  virtual void mouseReleaseEvent(QMouseEvent* event);
  virtual void focusInEvent(QFocusEvent* event);
  virtual void focusOutEvent(QFocusEvent* event);

private:
  QTimer Timer;
  QPoint LastPressPoint;
};

class BGScene : public QGraphicsScene
{
  Q_OBJECT
public:
  BGScene();
  virtual ~BGScene();

signals:
  void Leave();

public slots:
  virtual void Show();
  virtual void Hide();

protected:
  static BGView* View;
  int Width;
  int Height;
};

#endif
