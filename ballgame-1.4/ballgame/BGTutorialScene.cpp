/**
 *  This file is part of Ball Game
 *
 *  Copyright (C) 2011 Csaba Kertész (csaba.kertesz@gmail.com)
 *
 *  Ball Game is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  Ball Game is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 */

#include "BGTutorialScene.hpp"

#include "stdio.h"

#include <QApplication>

#include "BGAnimator.hpp"
#include "BGHelper.hpp"

BGTutorialScene::BGTutorialScene() : BGScene()
{
  // Tutorial
  TutorialTitle = new QGraphicsSimpleTextItem();
  QFont Font = TutorialTitle->font();

  Font.setPixelSize(50);
  TutorialTitle->setFont(Font);
  TutorialTitle->setPos(10.0, 10.0);
  TutorialTitle->setText("Tutorial");
  addItem(TutorialTitle);

  // Tutorial text
  TutorialText = new QGraphicsSimpleTextItem();
  Font = TutorialText->font();

  Font.setPixelSize(21);
  TutorialText->setFont(Font);
  TutorialText->setPos(10.0, 100.0);
  TutorialText->setText(QString("The target of the game to get a red ball to a hole. The ball can be hit in a specific\n")+
                                "direction by tapping/pressing over the ball and sliding to the desired direction. The\n"+
                                "final direction is determinated when the finger/mouse button is released. The force of\n"+
                                "the hit is determinated between the tap/press and the release points.\n"
                                "There are limited attempts to reach the hole with a ball; less attempts on higher levels\n"+
                                "and the game ends when the last ball is lost.\n"+
                                "Score is earned with each successful hit. If a ball kicks the wall before reaching\n"+
                                "the hole, extra score is given. Five kicks on the walls before the hole results\n"+
                                "an extra ball.\n"+
                                "The game contains five levels. One level is completed after five successful hits.\n"+
                                "The menu of the game scene can be activated by tapping/pressing and hold the same\n"+
                                "position for a half second.\n"+
                                "Enjoy the game!");
  addItem(TutorialText);

  // Main Menu button
  QPixmap* MainMenuButtonPixmap = LoadPixmap("main_menu_button.png");

  MainMenuButton = new BGButton;
  MainMenuButton->setPixmap(*MainMenuButtonPixmap);
  connect(MainMenuButton, SIGNAL(Clicked()), this, SIGNAL(Leave()));
  addItem(MainMenuButton);
  MainMenuButton->setPos(QPointF(Width-MainMenuButton->pixmap().width()-20, Height-MainMenuButton->pixmap().height()-20));
  delete MainMenuButtonPixmap;
}


BGTutorialScene::~BGTutorialScene()
{
  delete TutorialTitle;
  TutorialTitle = NULL;
  delete TutorialText;
  TutorialText = NULL;
  delete MainMenuButton;
  MainMenuButton = NULL;
}


void BGTutorialScene::ShowTutorial()
{
  TutorialTitle->setOpacity(0.0);
  TutorialTitle->show();
  new BGAnimator(*TutorialTitle, OpacityAnim, 1.0);

  TutorialText->setOpacity(0.0);
  TutorialText->show();
  new BGAnimator(*TutorialText, OpacityAnim, 1.0);

  MainMenuButton->Show();
}


void BGTutorialScene::HideTutorial()
{
  new BGAnimator(*TutorialTitle, OpacityAnim, 0.0, 300);
  new BGAnimator(*TutorialText, OpacityAnim, 0.0, 300);
  MainMenuButton->Hide();
}
