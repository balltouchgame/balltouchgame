# NOTE: The tablet version of the MeeGo does not contain the Phonon, so it is not in the dependencies.

# Define package name
%define app_name com.kecsap.ballgame
Name: ballgame
Version: 1.4
Release: 1
License: GPL2
Summary: Ball Game Application
Url: https://gitorious.org/balltouchgame/balltouchgame
Source: https://gitorious.org/balltouchgame/balltouchgame
Group: Amusements/Games
Requires: libqtcore4, libqtgui4
AutoReq: 0

%description
Ball Game

%prep
%setup -q

%build
qmake -recursive RPM_BUILD=on
qmake RPM_BUILD=on
make -j4

%install
make install INSTALL_ROOT=%{buildroot}

%post
if [ ! -e "/usr/lib/qt4/plugins/phonon_backend/phonon_gstreamer.so" ] ; then
	mkdir -p /usr/lib/qt4/plugins/phonon_backend/
	ln -s -f /opt/com.kecsap.ballgame/phonon_gstreamer.so /usr/lib/qt4/plugins/phonon_backend/phonon_gstreamer2.so
fi;
if [ ! -e "/usr/lib/libphonon.so.4" ] ; then
	ln -s -f /opt/com.kecsap.ballgame/libphonon.so.4.4.0 /usr/lib/libphonon.so.4
fi;

%postun
if [ -e "/usr/lib/qt4/plugins/phonon_backend/phonon_gstreamer2.so" ] ; then
	rm -rf /usr/lib/qt4/plugins/phonon_backend/phonon_gstreamer2.so
fi;

%files
%defattr(-,root,root,-)
/opt/%{app_name}/*
%{_datadir}/applications/*.desktop
%{_datadir}/icons/*.png
